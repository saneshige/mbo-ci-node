const validateUtil = require('../public/js/validateUtil');

describe('validateUtilのテスト', () => {

    describe('isEmpty()', () => {
        it('空文字を渡したとき、trueになること', () => {
            expect(validateUtil.isEmpty('')).toBeTruthy(); 
        });
        it('nullを渡したとき、trueになること', () => {
            expect(validateUtil.isEmpty(null)).toBeTruthy(); 
        });
        it('undefinedを渡したとき、trueになること', () => {
            expect(validateUtil.isEmpty(undefined)).toBeTruthy(); 
        });
        it('aを渡したとき、falseになること', () => {
            expect(validateUtil.isEmpty('a')).toBeFalsy(); 
        });
        it('0を渡したとき、falseになること', () => {
            expect(validateUtil.isEmpty(0)).toBeFalsy(); 
        });
    });

    describe('isNumber()', () => {
        it('空文字を渡したとき、falseになること', () => {
            expect(validateUtil.isNumber('')).toBeFalsy(); 
        });
        it('nullを渡したとき、falseになること', () => {
            expect(validateUtil.isNumber(null)).toBeFalsy(); 
        });
        it('undefinedを渡したとき、falseになること', () => {
            expect(validateUtil.isNumber(undefined)).toBeFalsy(); 
        });
        it('aを渡したとき、falseになること', () => {
            expect(validateUtil.isNumber('a')).toBeFalsy(); 
        });
        it('1,000を渡したとき、falseになること', () => {
            expect(validateUtil.isNumber('1,000')).toBeFalsy(); 
        });
        it('-1を渡したとき、falseになること', () => {
            expect(validateUtil.isNumber('-1')).toBeFalsy(); 
        });
        it('1.23を渡したとき、falseになること', () => {
            expect(validateUtil.isNumber('1.23')).toBeFalsy(); 
        });
        it('0を渡したとき、trueになること', () => {
            expect(validateUtil.isNumber('0')).toBeTruthy(); 
        });
    });

    describe('isEmail()', () => {
        it('空文字を渡したとき、falseになること', () => {
            expect(validateUtil.isEmail('')).toBeFalsy(); 
        });
        it('nullを渡したとき、falseになること', () => {
            expect(validateUtil.isEmail(null)).toBeFalsy(); 
        });
        it('undefinedを渡したとき、falseになること', () => {
            expect(validateUtil.isEmail(undefined)).toBeFalsy(); 
        });
        it('aを渡したとき、falseになること', () => {
            expect(validateUtil.isEmail('a')).toBeFalsy(); 
        });
        it('test@を渡したとき、falseになること', () => {
            expect(validateUtil.isEmail('test@')).toBeFalsy(); 
        });
        it('test@testを渡したとき、trueになること', () => {
            expect(validateUtil.isEmail('test@test')).toBeTruthy(); 
        });
        it('test@test.comを渡したとき、trueになること', () => {
            expect(validateUtil.isEmail('test@test.com')).toBeTruthy(); 
        });
    });
});