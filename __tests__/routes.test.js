const app = require('../app');
const request = require('supertest');

describe('routesのテスト', () => {

    describe('index', () => {
        it('200ステータスが帰ること', (done) => {
            request(app).get('/')
                .expect(200, done)
        });
    });

    describe('confirm', () => {
        it('200ステータスが帰ること', (done) => {
            request(app).post('/confirm')
                .expect(200, done);
        });
        it('エラーがある時、再入力の文言が表示されていること', (done) => {
            request(app).post('/confirm')
                .then((res) => {
                    expect(res.text).toContain('入力してください。');
                    done();
                });
        });
        it('エラーがない時、確認ページの文言が表示されていること', (done) => {
            request(app).post('/confirm')
                .send({ name: 'a', furigana: 'a', age: 9, experience: 10, tel: '0120', email: 'test@test.com' })
                .then((res) => {
                    expect(res.text).not.toContain('入力してください。');
                    done();
                });
        });
    });

    describe('confirmEnd', () => {
        it('200ステータスが帰ること', (done) => {
            request(app).post('/confirmEnd')
                .expect(200, done);
        });
        it('完了ページの文言が表示されること', (done) => {
            request(app).post('/confirmEnd')
                .then((res) => {
                    expect(res.text).toContain('登録が完了しました。');
                    done();
                });
        });
    });
});