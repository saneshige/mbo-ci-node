const express = require('express');
const router = express.Router();
const validator = require('../public/js/validateUtil');

router.post('/', function(req, res, next) {
  // console.log(req.body);
  const body = req.body;
  const errors = [];

  // 必須チェック
  if(validator.isEmpty(body.name)) {
    errors.push('名前を入力してください。');
  }
  if(validator.isEmpty(body.furigana)) {
    errors.push('ふりがなを入力してください。');
  }
  if(validator.isEmpty(body.age)) {
    errors.push('年齢を入力してください。');
  }
  if(validator.isEmpty(body.age)) {
    errors.push('年齢を入力してください。');
  }
  if(validator.isEmpty(body.experience)) {
    errors.push('経験年数を入力してください。');
  }
  if(validator.isEmpty(body.tel)) {
    errors.push('電話番号を入力してください。');
  }
  if(validator.isEmpty(body.email)) {
    errors.push('メールアドレスを入力してください。');
  }

  // 数値チェック
  if(!validator.isNumber(body.age)) {
    errors.push('年齢を半角数値で入力してください。');
  }
  if(!validator.isNumber(body.experience)) {
    errors.push('経験年数を半角数値で入力してください。');
  }

  // メールアドレスチェック
  if(!validator.isEmail(body.email)) {
    errors.push('メールアドレスを正しく入力してください。');
  }

  if(errors.length === 0) {
    res.render('confirm', { body: req.body });
  } else {
    res.render('index', { body: req.body, errors });
  }
});

module.exports = router;
