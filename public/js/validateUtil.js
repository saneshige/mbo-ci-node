
module.exports.isEmpty = (value) => {
    if(value === null || value === '' || value === undefined) return true;
    return false;
};

/*
正の整数のみtrueを返す
*/
module.exports.isNumber = (value) => {
    const regex = new RegExp(/^[0-9]+$/);
    return regex.test(value);
};

module.exports.isEmail = (value) => {
    const regex = new RegExp(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
    return regex.test(value);
};